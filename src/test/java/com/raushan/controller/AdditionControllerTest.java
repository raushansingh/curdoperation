package com.raushan.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.raushan.model.Addition;

import ch.qos.logback.core.boolex.Matcher;

@RunWith(MockitoJUnitRunner.class)
public class AdditionControllerTest {
	
	@InjectMocks
	AdditionController additionController=new AdditionController();
	
	@Mock
	Addition addition;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Ignore
	@Test                 
	public void additionTest() {
		Addition addition=new Addition();
		addition.setA(10);
		addition.setB(20);
		int sum=addition.getA()+addition.getB();
		//when(additionController.addPet(addition)).thenReturn(30);
		assertEquals(additionController.addPet(addition),30);
		
		
	}
	
}
