package com.raushan.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.raushan.model.Addition;
@Controller
@ResponseBody
@RequestMapping("/add")
public class AdditionController {
	@PostMapping("/number")
	public int addPet(@RequestBody Addition addition) {
		int firstNumber=addition.getA();
		int secondNumber=addition.getB();
     	int sum=firstNumber+secondNumber;	
		return sum;
	}
	
	

}

